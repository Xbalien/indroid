# This file is jni/Android.mk

LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_CFLAGS := -DDEBUG

# I want ARM, not thumb.
LOCAL_ARM_MODE := arm

# Name of the local module
LOCAL_MODULE    := testproc 
# The files that make up the source code
LOCAL_SRC_FILES :=  testproc.c 

include $(BUILD_EXECUTABLE)

