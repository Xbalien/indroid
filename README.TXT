				Indroid Readme
				_____________


Project:      Indroid - Thread Injection Kit
Version:      1.0
Author:       Aseem Jakhar
Organization: null - The open security community
Websites:     http://null.co.in
              http://nullcon.net
	      http://payatu.com
Copyright (c) 2011-2021 Aseem Jakhar <aseemjakhar_at_gmail.com>. All rights reserved.


A. Introduction
_______________

The aim of the project is to demonstrate that a simple debugging functionality on *nix systems a.k.a 
ptrace() can be abused by malware to inject malicious code in remote processes. Indroid provides
CreateRemoteThread() equivalent for ARM based *nix devices. 

If you want to get a more deeper insight into the working of the framework you may:
  1. Watch my Defcon 19 video on Jugaad - http://www.youtube.com/watch?v=vju6tq1lp0k
  2. Read the paper - http://www.slideshare.net/null0x00/project-jugaad

B. Howto
________
To use Indroid in a program all you need to do is compile the source. It creates libjugaad.a library which
you can link in your programs. You need to include jugaad.h in your code. The function that provides this
functionality is create_remote_thread() and is perhaps the only function required to be used.The header
file jugaad.h also explains the create_remote_thread() function and it's parameters in detail.


C. Compilation and instructions
____________

1. The directory testproc contains a simple test target program only used for testing.
2. The directory asm contains assembly code which was used to create the ARM shellcode for indroid.
3. The directory indroid contains all the code for indroid framework. 
4. You need the Android NDK setup and ndk-build script part of the $PATH
5. In the indroid directory run
  a. make 
     This will create the android binary for testing.
  b. For creating a static or shared library you need to change the Android.mk file accordingly.

6. The Makefile defaults to creating debug version of the library which has way too many printfs'.
   If you want to compile libjugaad for production, make sure you remove the CFLAGS '-DDEBUG' and '-g'
   in the Android.mk.

D. Next release
_______________

1. Add support for stealthy library injection.
2. Add support for remote process snooping. 
3. Add Dex injection support

E. Contribution
_______________

Feel free to send me an email (on aseemjakhar_at_gmail.com):
1. If you would like to contribute to indroid project.
2. If you would like to contribute to the null community in any way.

