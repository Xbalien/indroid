# This file is jni/Android.mk

LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_CFLAGS := -DDEBUG

# I want ARM, not thumb.
#LOCAL_ARM_MODE := arm

# Name of the local module
LOCAL_MODULE    := indroid
# The files that make up the source code
LOCAL_SRC_FILES :=  jugaad.c shellcode.c testjugaad.c 

include $(BUILD_EXECUTABLE)

